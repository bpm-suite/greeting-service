package am.iunetworks.bpmsuite.greetingservice.dto;

public class GreetingDto {
    public String id;
    public String greeting;
}
