package am.iunetworks.bpmsuite.greetingservice.handler;

import am.iunetworks.bpmsuite.greetingservice.messaging.AbstractMessageListener;
import am.iunetworks.bpmsuite.greetingservice.messaging.Message;
import am.iunetworks.bpmsuite.greetingservice.messaging.MessagingClient;
import am.iunetworks.bpmsuite.greetingservice.model.Greeting;
import am.iunetworks.bpmsuite.greetingservice.repository.GreetingRepository;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
public class SendGreetingHandler {
    @Autowired
    private MessagingClient messagingClient;

    @Autowired
    private GreetingRepository greetingRepository;

    @PostConstruct
    public void init() {
        messagingClient.addListener(new AbstractMessageListener("send-greeting.request", new HashMap<>()) {
            @Override
            public void messageReceived(Message message) {
                Map<String, Object> payload = message.getPayload();
                String greetingText = String.format("Hello, %s!", payload.get("recipient"));
                Greeting greeting = new Greeting(greetingText);
                greetingRepository.save(greeting);

                Map<String, Object> responsePayload = new HashMap<>();
                responsePayload.put("execution-id", payload.get("execution-id"));
                responsePayload.put("greeting", greetingText);
                Message responseMessage = new Message("send-greeting.response", responsePayload);
                messagingClient.sendMessage(responseMessage);
            }
        });
    }

    @Bean
    public NewTopic sendGreetingRequestTopic() {
        return new NewTopic("send-greeting.request", 1, (short) 1);
    }

    @Bean
    public NewTopic sendGreetingResponseTopic() {
        return new NewTopic("send-greeting.response", 1, (short) 1);
    }
}
