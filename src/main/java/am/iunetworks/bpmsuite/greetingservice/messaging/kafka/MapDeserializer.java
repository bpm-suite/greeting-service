package am.iunetworks.bpmsuite.greetingservice.messaging.kafka;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class MapDeserializer implements Deserializer<Map<String, Object>> {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Map<String, Object> deserialize(String s, byte[] bytes) {
        Map<String, Object> result = null;
        try {
            TypeReference<Map<String, Object>> typeRef = new TypeReference<>() {};
            result = objectMapper.readValue(bytes, typeRef);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
