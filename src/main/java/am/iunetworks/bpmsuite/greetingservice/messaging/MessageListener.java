package am.iunetworks.bpmsuite.greetingservice.messaging;

import java.util.Map;

public interface MessageListener {
    String getTopic();
    Map<String, Object> getFilter();
    void messageReceived(Message message);
}
