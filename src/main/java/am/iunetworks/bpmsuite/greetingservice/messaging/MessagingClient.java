package am.iunetworks.bpmsuite.greetingservice.messaging;

import java.util.Set;

public interface MessagingClient {
    void sendMessage(Message message);

    void addListener(MessageListener listener);
    void removeListener(MessageListener listener);
    Set<MessageListener> getListeners();
}
