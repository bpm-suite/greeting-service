package am.iunetworks.bpmsuite.greetingservice.messaging.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class MapSerializer implements Serializer<Map<String, Object>> {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public byte[] serialize(String s, Map<String, Object> map) {
        byte[] result = null;
        try {
            result = objectMapper.writeValueAsString(map).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
