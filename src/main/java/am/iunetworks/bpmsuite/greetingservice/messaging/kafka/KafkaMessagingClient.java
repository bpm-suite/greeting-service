package am.iunetworks.bpmsuite.greetingservice.messaging.kafka;

import am.iunetworks.bpmsuite.greetingservice.messaging.Message;
import am.iunetworks.bpmsuite.greetingservice.messaging.MessageListener;
import am.iunetworks.bpmsuite.greetingservice.messaging.MessagingClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class KafkaMessagingClient implements MessagingClient {
    private Set<MessageListener> listeners = new HashSet<>();

    @Autowired
    private KafkaTemplate<String, Map<String, Object>> kafkaTemplate;

    @Override
    public void sendMessage(Message message) {
        kafkaTemplate.send(message.getTopic(), message.getKey(), message.getPayload());
    }

    @KafkaListener(topicPattern = ".*")
    private void eventReceived(ConsumerRecord<String, Map<String, Object>> event) {
        System.out.println(String.format("***** Message received: Topic: %s, Key: %s, Payload: %s", event.topic(), event.key(), event.value()));
        notifyListeners(event);
    }

    @Override
    public void addListener(MessageListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(MessageListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<MessageListener> getListeners() {
        return listeners;
    }

    private void notifyListeners(ConsumerRecord<String, Map<String, Object>> event) {
        String topic = event.topic();
        String key = event.key();
        Map<String, Object> payload = event.value();
        Message message = new Message(event.topic(), key, payload);

        List<MessageListener> topicListeners = listeners.stream().filter(listener -> topic.equals(listener.getTopic())).collect(Collectors.toList());
        for (MessageListener listener: topicListeners) {
            Map<String, Object> filter = listener.getFilter();
            boolean notify = true;
            for (String filterKey: filter.keySet()) {
                if (!filter.get(filterKey).equals(payload.get(filterKey))) {
                    notify = false;
                    break;
                }
            }
            if (notify) {
                listener.messageReceived(message);
            }
        }

    }
}
