package am.iunetworks.bpmsuite.greetingservice.messaging;

import java.util.Map;

public abstract class AbstractMessageListener implements MessageListener {
    private String topic;
    private Map<String, Object> filter;

    public AbstractMessageListener(String topic, Map<String, Object> filter) {
        this.topic = topic;
        this.filter = filter;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    @Override
    public Map<String, Object> getFilter() {
        return filter;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setFilter(Map<String, Object> filter) {
        this.filter = filter;
    }
}
