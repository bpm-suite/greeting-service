package am.iunetworks.bpmsuite.greetingservice.controller;

import am.iunetworks.bpmsuite.greetingservice.dto.GreetingDto;
import am.iunetworks.bpmsuite.greetingservice.model.Greeting;
import am.iunetworks.bpmsuite.greetingservice.repository.GreetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/greetings")
public class GreetingController {
    @Autowired
    private GreetingRepository greetingRepository;

    @GetMapping
    public List<GreetingDto> getAllGreetings() {
        List<Greeting> greetingList = greetingRepository.findAll();
        return greetingList.stream().map(this::assembleGreetingDto).collect(Collectors.toList());
    }

    @PostMapping
    public GreetingDto addGreeting(@RequestBody String recipient) {
        Greeting greeting = new Greeting(recipient);
        greetingRepository.save(greeting);
        return assembleGreetingDto(greeting);
    }

    private GreetingDto assembleGreetingDto(Greeting greeting) {
        GreetingDto dto = new GreetingDto();
        dto.id = greeting.getId();
        dto.greeting = greeting.getGreeting();
        return dto;
    }
}
