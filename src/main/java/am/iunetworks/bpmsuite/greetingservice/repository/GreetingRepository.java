package am.iunetworks.bpmsuite.greetingservice.repository;

import am.iunetworks.bpmsuite.greetingservice.model.Greeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GreetingRepository extends JpaRepository<Greeting, String> {
}
